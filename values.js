export const A = [
  {
    id: 1,
    type: 'paragraph',
    children: [
      { id: 11, text: 'Here is some text' },
    ],
  }
]

export const B = [
  {
    id: 1,
    type: 'paragraph',
    children: [
      { id: 11, text: 'Here is some text' },
    ],
  }, 
  {
    id: 2,
    type: 'paragraph',
    children: [
      { id: 21, text: 'a new paragraph' },
    ],
  }
]

export const B1 = [ 
  {
    id: 2,
    type: 'paragraph',
    children: [
      { id: 21, text: 'a new paragraph' },
    ],
  }
]

export const C = [
  {
    id: 1,
    type: 'paragraph',
    children: [
      { id: 11, text: 'Here is some Text' },
    ],
  }
]

export const D = [
  {
    id: 1,
    type: 'paragraph',
    children: [
      { id: 11, text: 'text is some Here' },
    ],
  }
]

export const E = [
  {
    id: 1,
    type: 'paragraph',
    children: [
      { id: 11, text: 'Here are some texts' },
    ],
  }
]

export const F = [
  {
    id: 1,
    type: 'paragraph',
    children: [
      { id: 11, text: 'This is p1' },
    ],
  },
  {
    id: 2,
    type: 'paragraph',
    children: [
      { id: 21, text: 'This is p2' },
    ],
  },
  {
    id: 3,
    type: 'paragraph',
    children: [
      { id: 31, text: 'This is p3' },
    ],
  }
]

export const G = [
  {
    id: 1,
    type: 'paragraph',
    children: [
      { id: 11, text: 'This is p1 with new texts' },
    ],
  },
  {
    id: 4,
    type: 'paragraph',
    children: [
      { id: 41, text: 'This is new p2' },
    ],
  },
  {
    id: 3,
    type: 'paragraph',
    children: [
      { id: 31, text: 'This is p3' },
    ],
  }
]

export const table1 = [
  {
    type: 'table',
    id: 1,
    children: [
      {
        type: 'table-row',
        id: 11,
        children: [
          {
            type: 'table-cell',
            id: 111,
            children: [{ text: '', id: 1111 }],
          },
          {
            type: 'table-cell',
            id: 112,
            children: [{ text: 'Human', id: 1112 }],
          },
          {
            type: 'table-cell',
            id: 113,
            children: [{ text: 'Dog', id: 1113 }],
          },
          {
            type: 'table-cell',
            id: 114,
            children: [{ text: 'Cat', id: 1114 }],
          },
        ],
      },
      {
        type: 'table-row',
        id: 12,
        children: [
          {
            type: 'table-cell',
            id: 121,
            children: [{ text: '# of Feet', id: 1211 }],
          },
          {
            type: 'table-cell',
            id: 122,
            children: [{ text: '2', id: 1212 }],
          },
          {
            type: 'table-cell',
            id: 123,
            children: [{ text: '4', id: 1213 }],
          },
          {
            type: 'table-cell',
            id: 124,
            children: [{ text: '4', id: 1214 }],
          },
        ],
      },
      {
        type: 'table-row',
        id: 13,
        children: [
          {
            type: 'table-cell',
            id: 131,
            children: [{ text: '# of Lives', id: 1311 }],
          },
          {
            type: 'table-cell',
            id: 132,
            children: [{ text: '1', id: 1312 }],
          },
          {
            type: 'table-cell',
            id: 133,
            children: [{ text: '1', id: 1313 }],
          },
          {
            type: 'table-cell',
            id: 134,
            children: [{ text: '9', id: 1314 }],
          },
        ],
      },
    ],
  },
]

export const table2 = [
  {
    type: 'table',
    id: 1,
    children: [
      {
        type: 'table-row',
        id: 11,
        children: [
          {
            type: 'table-cell',
            id: 111,
            children: [{ text: '', id: 1111 }],
          },
          {
            type: 'table-cell',
            id: 112,
            children: [{ text: 'Huuuman', id: 1112 }],
          },
          {
            type: 'table-cell',
            id: 113,
            children: [{ text: 'Dog', id: 1113 }],
          },
          {
            type: 'table-cell',
            id: 114,
            children: [{ text: 'Cat', id: 1114 }],
          },
        ],
      },
      {
        type: 'table-row',
        id: 12,
        children: [
          {
            type: 'table-cell',
            id: 121,
            children: [{ text: '# of Feet', id: 1211 }],
          },
          {
            type: 'table-cell',
            id: 122,
            children: [{ text: '2', id: 1212 }],
          },
          {
            type: 'table-cell',
            id: 123,
            children: [{ text: '4', id: 1213 }],
          },
          {
            type: 'table-cell',
            id: 124,
            children: [{ text: '4', id: 1214 }],
          },
        ],
      },
      {
        type: 'table-row',
        id: 13,
        children: [
          {
            type: 'table-cell',
            id: 131,
            children: [{ text: '# of Forms', id: 1311 }],
          },
          {
            type: 'table-cell',
            id: 132,
            children: [{ text: '1', id: 1312 }],
          },
          {
            type: 'table-cell',
            id: 133,
            children: [{ text: '7', id: 1313 }],
          },
          {
            type: 'table-cell',
            id: 134,
            children: [{ text: '9', id: 1314 }],
          },
        ],
      },
      {
        type: 'table-row',
        id: 14,
        children: [
          {
            type: 'table-cell',
            id: 141,
            children: [{ text: '# of Word', id: 1411 }],
          },
          {
            type: 'table-cell',
            id: 142,
            children: [{ text: '4', id: 1412 }],
          },
          {
            type: 'table-cell',
            id: 143,
            children: [{ text: '7', id: 1414 }],
          },
          {
            type: 'table-cell',
            id: 144,
            children: [{ text: '2', id: 1414 }],
          },
        ],
      },
    ],
  },
]