import React, { useCallback, useMemo, useState } from 'react'
import { Editable, withReact, Slate } from 'slate-react'
import { createEditor } from 'slate'
import { render } from "react-dom"
import zip from "lodash.zip"
import { A, B, B1, C, D, E, F, G, table1, table2 } from "./values"

const App = () => {
  const [valueBefore, setValueBefore] = useState(A)
  const [valueAfter, setValueAfter] = useState(A)
  const [valueDiff, setValueDiff] = useState([])
  const runDiff = () => {
    const o = diff(valueBefore, valueAfter)
    setValueDiff(o)
  }
  return (
    <div>
      <label>Select from values</label>
      <select onChange={e => {
        switch(e.target.value) {
          case "0":
            setValueBefore(A)
            setValueAfter(B)
            break
          case "1":
            setValueBefore(A)
            setValueAfter(B1)
            break
          case "2":
            setValueBefore(A)
            setValueAfter(C)
            break
          case "3":
            setValueBefore(A)
            setValueAfter(D)
            break
          case "4":
            setValueBefore(A)
            setValueAfter(E)
            break
          case "5":
            setValueBefore(F)
            setValueAfter(G)
            break
          case "6":
            setValueBefore(table1)
            setValueAfter(table2)
            break
          default: 
            setValueBefore(A)
            setValueAfter(A)
            break
        }
      }}>
        <option value={-1}>---SELECT---</option>
        <option value={0}>insert a new block below</option>
        <option value={1}>delete first and insert a new block</option>
        <option value={2}>modify text(1)</option>
        <option value={3}>modify text(2)</option>
        <option value={4}>modify text(3)</option>
        <option value={5}>complex</option>
        <option value={6}>table</option>
      </select>
      <button onClick={runDiff}>diff</button>
      <div>
        <label>Verson 1</label>
        <DiffExample value={valueBefore}/>
      </div>
      <div>
        <label>Verson 2</label>
        <DiffExample value={valueAfter}/>
      </div>
      <DiffExample value={valueDiff}/>
    </div>
  )
}
const DiffExample = (props) => {
  const renderElement = useCallback(props => <Element {...props} />, [])
  const renderLeaf = useCallback(props => <Leaf {...props} />, [])
  const editor = useMemo(() => withTables(withReact(createEditor())), [])
  window.editor = editor

  return (
    <div className="editor">
      <Slate editor={editor} value={props.value} onChange={() => {}}>
      <Editable
        renderElement={renderElement}
        renderLeaf={renderLeaf}
        spellCheck
        autoFocus
        readOnly
      />
    </Slate>
    </div>
  )
}

const withTables = editor => {
  const { deleteBackward, deleteForward, insertBreak } = editor

  editor.deleteBackward = unit => {
    const { selection } = editor

    if (selection && Range.isCollapsed(selection)) {
      const [cell] = Editor.nodes(editor, {
        match: n => n.type === 'table-cell',
      })

      if (cell) {
        const [, cellPath] = cell
        const start = Editor.start(editor, cellPath)

        if (Point.equals(selection.anchor, start)) {
          return
        }
      }
    }

    deleteBackward(unit)
  }

  editor.deleteForward = unit => {
    const { selection } = editor

    if (selection && Range.isCollapsed(selection)) {
      const [cell] = Editor.nodes(editor, {
        match: n => n.type === 'table-cell',
      })

      if (cell) {
        const [, cellPath] = cell
        const end = Editor.end(editor, cellPath)

        if (Point.equals(selection.anchor, end)) {
          return
        }
      }
    }

    deleteForward(unit)
  }

  editor.insertBreak = () => {
    const { selection } = editor

    if (selection) {
      const [table] = Editor.nodes(editor, { match: n => n.type === 'table' })

      if (table) {
        return
      }
    }

    insertBreak()
  }

  return editor
}

const Element = ({ attributes, children, element }) => {
  const backgroundColor = element.inserted
    ? "green"
    : element.deleted
      ? "red"
      : undefined

  switch (element.type) {
    case 'table':
      return (
        <table>
          <tbody {...attributes} style={{backgroundColor}}>{children}</tbody>
        </table>
      )
    case 'table-row':
      return <tr {...attributes} style={{backgroundColor}}>{children}</tr>
    case 'table-cell':
      return <td {...attributes} style={{backgroundColor}}>{children}</td>
    default:
      return <p {...attributes} style={{backgroundColor}}>{children}</p>
  }
}

const Leaf = ({ attributes, children, leaf }) => {
  if (leaf.bold) {
    children = <strong>{children}</strong>
  }

  if (leaf.code) {
    children = <code>{children}</code>
  }

  if (leaf.italic) {
    children = <em>{children}</em>
  }

  if (leaf.underline) {
    children = <u>{children}</u>
  }

  return (
    <span {...attributes}
      style={{
        color: leaf.inserted 
          ? "green"
          : leaf.deleted
            ? "red"
            : undefined,
        textDecoration: leaf.deleted ? "line-through" : undefined}}>
      {children}
    </span>
  )
}

const diff = (a, b) => zip(a,b)
  .map((pair) => {
    const [aa, bb] = pair
    if (aa != undefined && bb != undefined) {
      if (aa.id === bb.id) {
        if (aa.children && bb.children)
          return [{
            ...aa,
            children: diff(aa.children, bb.children)
          }]
        else if (aa.children) {

        }
        else if (bb.children) {

        }
        else {
          return diffText(aa, bb)
        }
      }
      else {
        const ca = {...aa, deleted: true}
        const cb = {...bb, inserted: true}
        return [ca, cb]
      }
    }
    else if (aa != undefined)
      return [{...aa, deleted: true}]
    else
      return [{...bb, inserted: true}]
  })
  .flat()

function index(arr) {
  return (i) => (arr.length + i) % arr.length
}

function buildPaths(a, b) {
  const la = a.length
  const lb = b.length
  const max = la + lb
  const paths = []
  const lv = 2 * max + 1
  const v = Array(lv).fill(0)
  const ind = index(v)

  for (let d = 0; d <= max; d += 1) {
    for (let k = -d; k <= d; k += 2) {
      let x = 0
      if (k == -d || (k != d && v[ind(k+1)] > v[ind(k-1)])) {
        x = v[ind(k + 1)]
      }
      else {
        x = v[ind(k-1)] + 1
      }
      let y = x - k
      while (x < la && y < lb && a.charAt(x) == b.charAt(y)) {
        x += 1
        y += 1
      }
      v[ind(k)] = x
      if (x == la && y == lb) {
        paths.push([...v])
        return paths
      }
    }
    paths.push([...v])
  }
  return paths
}

function backtrack(a, b) {
  let x = a.length
  let y = b.length
  const paths = buildPaths(a, b).reverse()
  const ind = index(paths[0])

  const arr = []
  for (let i = 0; i < paths.length; i++) {
    const k = x - y
    const v = paths[i]
    let prevk = 0
    const d = paths.length - 1 - i
    if (k == -d || (k != d && v[ind(k+1)] > v[ind(k-1)])) {
      prevk = k + 1
    }
    else {
      prevk = k - 1
    }
    let prevx = v[ind(prevk)]
    let prevy = prevx - prevk
    arr.push([x,y])
    while(x > prevx && y > prevy) {
      x -= 1
      y -= 1
      arr.push([x, y])
    }
    x = prevx
    y = prevy
  }
  return arr
}

function diffText(a, b) {
  const steps = backtrack(a.text, b.text).reverse()
  const result = []
  for (let i = 0; i < steps.length - 1; i++) {
    const [x,y] = steps[i]
    const [xx,yy] = steps[i+1]
    if (x == xx) {
      result.push({ text: b.text.charAt(y), inserted: true })
    }
    else if (y == yy) {
      result.push({ text: a.text.charAt(x), deleted: true })
    }
    else {
      result.push({ text: a.text.charAt(x) })
    }
  }
  return result.length == 0 ? [{ text: '' }] : result
}

render(<App />, document.getElementById("app"))